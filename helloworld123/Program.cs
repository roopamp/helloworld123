﻿// A Hello World! program in C#.
using HelloWorld1;
using System;
using System.Collections.Generic;
using System.Linq;

namespace HelloWorld
{
    class Hello
    {
        static void Main()
        {
            Console.WriteLine("Hello World!");


            #region Dino Stuff

            Console.WriteLine("---------------------DINO STUFF BEGINS ---------------------");

            //create a basic dino object.
            var tempDinosaur1 = new Dinosaur();

            var tempDinoString = tempDinosaur1.ToString();

            Console.WriteLine(tempDinoString);


            //create a basic dino object. - part 2 with custom constructor

            string tempDinoName = "Triceratops";
            int tempDinoHeight = 10;
            int tempDinoWeight = 12;
            string tempDinoTerrain = "North America";
            int tempDinoID = 1;

            var tempDinosaur2 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            var tempDinoString2 = tempDinosaur2.ToString();

            //var tempDinosaur3 = new Dinosaur();
            //tempDinosaur3 = tempDinosaur3.Input();
            //tempDinosaur3.Display();

            //var tempDinoString3 = tempDinosaur3.ToString();

            #endregion

            #region Dino Stuff 2

            //TODO - Collections LINQ and lambda 

            //add a unique id to each dino.

            //unique id done. 

            //Collection of Dino Stuff

            var CollectionOfDino = new List<Dinosaur>();

            CollectionOfDino.Add(tempDinosaur1);
            CollectionOfDino.Add(tempDinosaur2);

            //we need some ten dinos. so 8 more.
            CollectionOfDino = AddTenDinos();


            //CollectionOfDino.Add(tempDinosaur3);

            //display the collection
            DinoInputFiveDinos(CollectionOfDino);
            DisplayDinoCollection(CollectionOfDino);
            DeleteDino(CollectionOfDino);

            //lets do some srot

            var CollectionOfDino_sorted = CollectionOfDino.OrderByDescending(x => x.DinoHeight).ToList(); ;
            DisplayDinoCollection(CollectionOfDino_sorted);

            //show them based on terrain. 

            ShowDinoBasedOnCountry(CollectionOfDino);

            //search for dinos based on a country. 
            SearchForDinoBasedOnCountry(CollectionOfDino);

            //Add some dinos to this collection

            //make sure that no dino has more than one id.

            //after every addition show the collection

            //search a dino based on id

            //remove a dino. 

            //

            #endregion

            #region basic function stuff

            //var name = "Jay";
            //f1();
            //f2(name);
            //var message = f3(name);
            //Console.WriteLine("In main function " + message);

            #endregion


            Console.WriteLine("Press any key to exit.");
            Console.ReadKey();
        }

        //this will take a region from user
        //then show dinos that belong to that user.
        private static void DeleteDino(List<Dinosaur> collectionOfDino)
        {
            var message = "";

            //display message 
            message = "Enter a id and we will delete that dino.";
            Console.WriteLine(message);
            var enteredDinoID = Console.ReadLine();
            var DinoIDNumber = Convert.ToInt32(enteredDinoID);

            //find the dino with the entered id. 
            var DinoFound = collectionOfDino.Select(x => x).Where(x => x.DinoID == DinoIDNumber).FirstOrDefault();

            if (DinoFound == null)
            {
                //display message 
                message = "Dino with ID - " + DinoIDNumber + "does not exist in our collection";
                Console.WriteLine(message);
            }
            else
            {
                collectionOfDino.Remove(DinoFound);
                message = "Dino with ID - " + DinoIDNumber + "has been deleted";
                Console.WriteLine(message);
            }

            DisplayDinoCollection(collectionOfDino);
        }
        private static List<Dinosaur> DinoInputFiveDinos(List<Dinosaur> collectionOfDino)
        {
            int numberOfDinosToEnter = 2;
            //throw new NotImplementedException();
            var message = "";

            //display message 
            message = "Now, time to add " + numberOfDinosToEnter + "dinos to the collection.";
            Console.WriteLine(message);

            for (int i = 0; i < numberOfDinosToEnter; i++)
            {

                //display message 
                message = "Add dino details ";
                Console.WriteLine(message);

                //lets do the input
                //some temporary values to which we will fill user entered values.
                string tempDinoName = "Triceratops";
                int tempDinoHeight = 10;
                int tempDinoWeight = 12;
                string tempDinoTerrain = "North America";
                int tempDinoID = 1;

                //collect input from user


                message = "enter dino name";
                Console.WriteLine(message);
                tempDinoName = Console.ReadLine();

                message = "enter dino height";
                Console.WriteLine(message);
                var tempNumber = Console.ReadLine();
                tempDinoHeight = Convert.ToInt32(tempNumber);

                message = "enter dino weight";
                Console.WriteLine(message);
                var tempNumber2 = Console.ReadLine();
                tempDinoWeight = Convert.ToInt32(tempNumber2);

                message = "enter dino terrain";
                Console.WriteLine(message);
                tempDinoTerrain = Console.ReadLine();

                message = "enter dino id";
                Console.WriteLine(message);
                var tempNumber3 = Console.ReadLine();
                tempDinoID = Convert.ToInt32(tempNumber3);

                //check if ID is unique. 
                var FindDino = collectionOfDino.Select(x => x).Where(x => x.DinoID == tempDinoID).FirstOrDefault();

                if (FindDino != null)
                {
                    //this means, dino with that id is already in our collection. 
                    //get the id of the last dino in our collection.
                    var lastDino = collectionOfDino.Last();
                    //get the id of the last dino
                    var lastDinoID = lastDino.DinoID;
                    //increase this by 1 and make it the id of the new dino.
                    tempDinoID = lastDinoID + 1;
                }

                //fill it up to a dino object.
                //create a dino object to return after getting all the values from the user. 

                var toReturnDino = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

                //add this new dino to our collection. 
                collectionOfDino.Add(toReturnDino);

                //display message 
                message = "Dino with ID " + toReturnDino.DinoID + " added to our collection";
                Console.WriteLine(message);
            }//end of for loop

            //return the collection with the newly added dinos.
            return collectionOfDino;

        }

        //this will take a region from user
        //then show dinos that belong to that user.
        private static void SearchForDinoBasedOnCountry(List<Dinosaur> collectionOfDino)
        {
            var message = "";

            //display message asking for dino region
            message = "Enter the region code - IN or CH or AM";
            Console.WriteLine(message);

            var regioncode = Console.ReadLine();

            var countrycode = regioncode;
            var DinoResultList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == countrycode).ToList();

            DisplayDinoCollection(DinoResultList);
        }

        //this function shows dinos based on the country coude.
        private static void ShowDinoBasedOnCountry(List<Dinosaur> collectionOfDino)
        {
            //sort collection based on country code. 
            var countrycode = "IN";
            var DinoIndiaList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == countrycode).ToList();
            countrycode = "AM";
            var DinoAmericaList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == countrycode).ToList();
            countrycode = "CH";
            var DinoChinaList = collectionOfDino.Select(x => x).Where(x => x.DinoTerrain == countrycode).ToList();

            DisplayDinoCollection(DinoIndiaList);
            DisplayDinoCollection(DinoAmericaList);
            DisplayDinoCollection(DinoChinaList);

            var something = 10;
        }

        //this is a simple database builds
        //adds ten dinos and returns that collection
        private static List<Dinosaur> AddTenDinos()
        {
            string tempDinoName = "Triceratops";
            int tempDinoHeight = 10;
            int tempDinoWeight = 12;
            string tempDinoTerrain = "IN";
            int tempDinoID = 1;

            var Dino1 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 17;
            tempDinoWeight = 12;
            tempDinoTerrain = "IN";
            tempDinoID = 2;

            var Dino2 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 40;
            tempDinoWeight = 12;
            tempDinoTerrain = "IN";
            tempDinoID = 3;

            var Dino3 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 10;
            tempDinoWeight = 98;
            tempDinoTerrain = "AM";
            tempDinoID = 4;

            var Dino4 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 53;
            tempDinoWeight = 12;
            tempDinoTerrain = "AM";
            tempDinoID = 5;

            var Dino5 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 88;
            tempDinoWeight = 12;
            tempDinoTerrain = "AM";
            tempDinoID = 6;

            var Dino6 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 49;
            tempDinoWeight = 12;
            tempDinoTerrain = "CH";
            tempDinoID = 7;

            var Dino7 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 69;
            tempDinoWeight = 12;
            tempDinoTerrain = "CH";
            tempDinoID = 8;

            var Dino8 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 71;
            tempDinoWeight = 12;
            tempDinoTerrain = "CH";
            tempDinoID = 9;

            var Dino9 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            tempDinoName = "Triceratops";
            tempDinoHeight = 90;
            tempDinoWeight = 12;
            tempDinoTerrain = "CH";
            tempDinoID = 10;

            var Dino10 = new Dinosaur(tempDinoName, tempDinoHeight, tempDinoWeight, tempDinoTerrain, tempDinoID);

            //tempDinoName = "Triceratops";
            //tempDinoHeight = 30;
            //tempDinoWeight = 12;
            //tempDinoTerrain = "North America";
            //tempDinoID = 1;

            //add all dinos to a collection

            var toReturnDinoCollectiono = new List<Dinosaur>();
            toReturnDinoCollectiono.Add(Dino1);
            toReturnDinoCollectiono.Add(Dino2);
            toReturnDinoCollectiono.Add(Dino3);
            toReturnDinoCollectiono.Add(Dino4);
            toReturnDinoCollectiono.Add(Dino5);
            toReturnDinoCollectiono.Add(Dino6);
            toReturnDinoCollectiono.Add(Dino7);
            toReturnDinoCollectiono.Add(Dino8);
            toReturnDinoCollectiono.Add(Dino9);
            toReturnDinoCollectiono.Add(Dino10);

            //return the collection
            return toReturnDinoCollectiono; ;
        }

        //this shows any dino collection that is sent
        //along with number of dinos
        private static void DisplayDinoCollection(List<Dinosaur> collectionOfDino)
        {
            //throw new NotImplementedException();

            //get total dinos.
            var totalDinos = collectionOfDino.Count;

            var message = "";

            //display total dinos for reference
            message = "Total Number of Dinos - " + totalDinos;
            Console.WriteLine(message);

            //loop through each dino.
            foreach (var dino in collectionOfDino)
            {
                //display dino details using the already existing display function
                dino.Display();

                //put a simple line to indicate that a new dino will be displated in the next iteration
                message = "--------------------";
                Console.WriteLine(message);
            }
        }

        //this demos some basic type stuff
        static void BasicTypeStuff()
        {


            #region basic int stuff

            //int a = 5;
            //int b = a + 2; //OK

            //bool test = true;

            //// Error. Operator '+' cannot be applied to operands of type 'int' and 'bool'.
            //int c = a + test;

            // Keep the console window open in debug mode.

            #endregion

            #region basic string stuff

            String string1 = "Exciting times ";
            String string2 = "lie ahead of us";

            String combineTheTwoStrings = string1 + string2;

            Console.WriteLine(combineTheTwoStrings);

            #endregion

            #region basic bool stuff. 

            //taking input
            Console.WriteLine("Enter a number please");
            //var input = Console.ReadLine().ToString();
            var input = "5";

            //converting the input (it will be in string format) to a int type

            int number = 0;
            try
            {
                number = Convert.ToInt32(input);
            }
            catch (Exception e)
            {
                Console.WriteLine("Got some error - {0}", e.ToString());
                //assign a default number in case of error to resume code flow
                number = 10;
            }


            //we need a bool flag. 
            bool flag;

            if (number > 5)
            {
                flag = true;
            }
            else
            {
                flag = false;
            }

            //lets display the value of the flag in the output.
            Console.WriteLine("The value of flag is {0}", flag);

            #endregion

        }

        //first scenario function
        //one way communication without any parameters
        static void f1()
        {
            var message = "I am in function 1";
            Console.WriteLine(message);
        }

        //one way communication with parameters
        static void f2(string name)
        {
            var message = "Helo " + name + ", I am in function 2";
            Console.WriteLine(message);
        }

        //two way communication with parameters. 
        static string f3(string name)
        {
            var message = "Helo " + name + ", I am in function 3";
            //Console.WriteLine(message);
            return message;
        }
    }
}